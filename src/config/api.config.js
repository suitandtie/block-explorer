const Api = {
  apiList: [],

  daemonAddress: "https://explorer.galaxia-project.com/api",
  apiGetInfo: "/getinfo.php",
  apiJsonRpc: "/jsonrpc.php",

  // daemonAddress: "http://127.0.0.1:22869",
  // apiGetInfo: "/getinfo",
  // apiJsonRpc: "/json_rpc",

  jsonRpcBase: {
    jsonrpc: "2.0"
  }
};

export {
  Api
};