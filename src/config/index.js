import {
    App
} from './app.config';
import {
    Api
} from './api.config';
import {
    Coin
} from './coin.config';

const configureWebpack = {
    devtool: 'source-map'
}

export {
    App,
    Api,
    Coin,
    configureWebpack,
};