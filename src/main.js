import '@fortawesome/fontawesome-free/css/all.min.css';
import 'typeface-source-sans-pro';
import 'typeface-roboto-mono';
import Vue from 'vue';
import App from './App.vue';
import router from '@/router';
import store from '@/store';
import filters from '@/filters';
import '@/components';

Vue.config.productionTip = false;

Vue.use(filters)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');