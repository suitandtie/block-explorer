# Forking the Xi Block Explorer
The Xi block explorer should be compatible with many CryptoNote forks.

There may be some Xi-specific rpc features, but legacy fallbacks have been included for compatibility.

## Project Setup
Fork and clone the project.

Install the vue cli tools.
```bash
npm install -g @vue/cli
```

In the project root directory, run a local dev server with
```bash
npm run dev
```

or build for production with
```bash
npm run build
```

## Forking
### Coin Settings
Set coin configuration in `/src/config/coin.config.js`.

Xi has fixed some inconsistencies with block index vs. height in the daemon RPC calls.
If your project doesn't have these changes, you may need to set `heightIndexOffset: -1`.

### Daemon API
Point the block explorer at your daemon in `/src/config/api.config.js`.
We run our https requests through Google cloud functions.

If you're connecting directly to a daemon, you may have to stringify your
request bodies in `/src/services/Blockchain/blockchain.service.js`.


### Branding
Replace `/src/assets/logo.png` with your logo.
Replace `/public/favicon.ico` with your favicon.
Update the page title in `/public/index.html`.

The rest is just css styling.

### Paper Wallet
For the paper wallet, update `/src/scripts/crypto_utils.js` config section for now.

## Deployment
Build for production.
```bash
npm run build
```
This creates a dist folder in the project root. Host this with any web server.
https://router.vuejs.org/guide/essentials/history-mode.html#example-server-configurations
